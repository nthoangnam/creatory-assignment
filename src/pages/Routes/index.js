import { LogoutOutlined, OrderedListOutlined } from "@ant-design/icons";
import { Layout, Menu } from "antd";
import React from "react";
import { Redirect, Route, Router, Switch } from "react-router-dom";
import logo from "../../assets/images/creatory-logo.png";
import history from "../../common/history";
import { routersNotAuth } from "../../configs";
const { Header, Sider, Content } = Layout;

const Routes = (props) => {
  const Auth = () => {
    if (!localStorage.getItem("access-token")) {
      return false;
    }
    if (localStorage.getItem("access-token")) {
      return true;
    }
  };

  const logOut = () => {
    localStorage.clear();
    history.push("/login");
  };

  return (
    <Router history={history}>
      <Switch>
        {routersNotAuth.map((route, idx) => (
          <Route
            key={idx}
            exact={route.exact}
            path={route.path}
            render={(props) => {
              const Component = React.lazy(() =>
                import(`../../pages/${route.component}`)
              );
              return (
                <React.Suspense fallback={null}>
                  {Auth() ? <Redirect to="/" /> : <Component auth={Auth} />}
                </React.Suspense>
              );
            }}
          />
        ))}
        <Route
          path="/"
          render={(props) => {
            const Component = React.lazy(() => import("../App"));
            return (
              <React.Suspense fallback={null}>
                {Auth() ? (
                  <Layout>
                    <Sider trigger={null}>
                      <div className="logo">
                        <img alt="creatory-logo" src={logo} />
                      </div>
                      <Menu
                        theme="dark"
                        mode="inline"
                        defaultSelectedKeys={["1"]}
                      >
                        <Menu.Item
                          key="1"
                          icon={<OrderedListOutlined />}
                          onClick={() => {
                            history.push("/all-record");
                          }}
                        >
                          All record
                        </Menu.Item>
                        <Menu.Item
                          key="2"
                          icon={<LogoutOutlined />}
                          onClick={() => logOut()}
                        >
                          Log out
                        </Menu.Item>
                      </Menu>
                    </Sider>
                    <Layout className="site-layout">
                      <Header
                        className="site-layout-background header"
                        style={{ padding: 0 }}
                      >
                        {" "}
                        <div className="date-list-title">All record:</div>
                      </Header>
                      <Content
                        className="site-layout-background"
                        style={{
                          margin: "24px 16px",
                          padding: 24,
                          minHeight: 280,
                        }}
                      >
                        <Component />
                      </Content>
                    </Layout>
                  </Layout>
                ) : (
                  <Redirect to={routersNotAuth[0].path} />
                )}
              </React.Suspense>
            );
          }}
        />
      </Switch>
    </Router>
  );
};

export default Routes;
