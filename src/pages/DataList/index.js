import { Pagination, Popover, Table, Tag, Tooltip } from "antd";
import axios from "axios";
import GoogleMapReact from "google-map-react";
import React, { useEffect, useState } from "react";
import { ReactComponent as EyeColor } from "../../assets/images/icons/eye.svg";
import ggMap from "../../assets/images/icons/gg-map-icon.png";
import maker from "../../assets/images/icons/maker.png";
import man from "../../assets/images/icons/man.png";
import menu from "../../assets/images/icons/menu.png";
import women from "../../assets/images/icons/woman.png";
import yes from "../../assets/images/icons/yes.png";
import no from "../../assets/images/icons/no.png";
import Loading from "../../components/loading";

import "./index.scss";

const Position = ({ text }) => (
  <div>
    {<img alt="maker" src={maker} style={{ width: "30px", height: "30px" }} />}
  </div>
);

const DataList = () => {
  const [currentPage, setCurrentPage] = useState(1);
  const [recordPerPage] = useState(1000);
  const [limit] = useState(10);
  const [currentData, setCurrentData] = useState([]);
  const [open, setOpen] = useState(false);

  const columns = [
    {
      title: "No",
      dataIndex: "id",
      key: "id",
      render: (Number) => <span>{Number + 1}</span>,
    },
    {
      title: "First name",
      dataIndex: "firstName",
      key: "firstName",
      render: (text) => <span>{text}</span>,
    },
    {
      title: "Last name",
      dataIndex: "lastName",
      key: "lastName",
      render: (text) => <span>{text}</span>,
    },
    {
      title: "Gender",
      dataIndex: "gender",
      key: "gender",
      render: (text) => (
        <img
          alt="gender"
          src={text === "female" ? women : man}
          style={{ width: "30px" }}
        />
      ),
    },
    {
      title: "Age",
      dataIndex: "age",
      key: "age",
    },
    {
      title: "Email",
      dataIndex: "email",
      key: "email",
    },
    {
      title: "Phone",
      dataIndex: "phone",
      key: "phone",
    },
    {
      title: "About",
      dataIndex: "about",
      key: "about",
      ellipsis: {
        showTitle: false,
      },
      render: (about) => (
        <Tooltip placement="topLeft" title={about}>
          {about.length > 50 ? about.substring(0, 37) + "..." : about}
        </Tooltip>
      ),
    },
    {
      title: "Eye color",
      dataIndex: "eyeColor",
      key: "eyeColor",
      render: (text) => (
        <div
          style={{
            width: "100%",
            display: "flex",
            justifyContent: "center",
            textAlign: "center",
          }}
        >
          <EyeColor fill={text} width={30} />
        </div>
      ),
    },
    {
      title: "Balance",
      dataIndex: "balance",
      key: "balance",
    },
    {
      title: "Active",
      dataIndex: "isActive",
      key: "isActive",
      render: (Boolean) => (
        <span>
          {Boolean === true ? (
            <img alt="yes" src={yes} style={{ width: "30px" }} />
          ) : (
            <img alt="no" src={no} style={{ width: "30px" }} />
          )}
        </span>
      ),
    },
    {
      title: "Registered",
      dataIndex: "registered",
      key: "registered",
    },
    {
      title: "Position",
      dataIndex: "coordinate",
      key: "coordinate",
      render: (coordinate) => (
        <Popover
          trigger="click"
          title="Map"
          placement="leftTop"
          content={
            <div style={{ height: "800px", width: "800px" }}>
              <GoogleMapReact
                bootstrapURLKeys="AIzaSyCvR_BIqJ-fIJIRISPpsPU2VnFxmp0PAbc"
                defaultCenter={{
                  lat: coordinate.latitude,
                  lng: coordinate.longtitude,
                }}
                defaultZoom={11}
              >
                <Position
                  lat={coordinate.latitude}
                  lng={coordinate.longtitude}
                  text="My Marker"
                />
              </GoogleMapReact>
            </div>
          }
        >
          {" "}
          <img
            alt="google-map"
            src={ggMap}
            style={{ width: "30px", cursor: "pointer" }}
          />
        </Popover>
      ),
    },
    {
      title: "Tags",
      key: "tags",
      dataIndex: "tags",
      render: (tags) => (
        <Popover
          placement="leftTop"
          content={tags.map((tag) => {
            return (
              <Tag color={"#ec6864"} key={tag}>
                {tag.toUpperCase()}
              </Tag>
            );
          })}
          title="Tags"
        >
          <img alt="" src={menu} style={{ cursor: "pointer" }} />
        </Popover>
      ),
    },
  ];

  useEffect(() => {
    setOpen(true)
    const fetchDataFirstTime = () => {
      return axios({
        method: "get",
        url: "http://localhost:8888/data",
        params: {
          offset: 0,
          limit: recordPerPage,
        },
        headers: {},
      })
        .then((res) => {
          let currentDataArr = [];
          let currentDataArrCoordinate = [];
          for (let i = 0; i < limit; i++) {
            currentDataArr.push(res.data[i]);
          }
          currentDataArr = currentDataArr.map((item, idx) => {
            let element = {};
            Object.entries(item).map(([key, value], idx) => {
              if (key !== "latitude" && key !== "longitude") {
                element[key] = value;
              }
              return null;
            });
            element["coordinate"] = {
              latitude: item["latitude"],
              longtitude: item["longitude"],
            };
            currentDataArrCoordinate.push(element);
            return null;
          });
          setCurrentData(currentDataArrCoordinate);
          setOpen(false)
        })
        .catch((err) => {
          setOpen(false)
          console.log(err);
        });
    };
    fetchDataFirstTime();
    // eslint-disable-next-line react-hooks/exhaustive-deps
  }, []);

  const fetchData = (e) => {
    setOpen(true)
    setCurrentPage(e);
    axios({
      method: "get",
      url: "http://localhost:8888/data",
      params: {
        offset: parseInt(e - 1) * limit,
        limit: limit,
      },
      headers: {},
    })
      .then((res) => {
        let currentDataArr = [];
        let currentDataArrCoordinate = [];
        for (let i = 0; i < limit; i++) {
          currentDataArr.push(res.data[i]);
        }
        currentDataArr = currentDataArr.map((item, idx) => {
          let element = {};
          Object.entries(item).map(([key, value], idx) => {
            if (key !== "latitude" && key !== "longitude") {
              element[key] = value;
            }
            return null;
          });
          element["coordinate"] = {
            latitude: item["latitude"],
            longtitude: item["longitude"],
          };
          currentDataArrCoordinate.push(element);
          return null;
        });
        setCurrentData(currentDataArrCoordinate);
        setOpen(false)
      })
      .catch((err) => {
        setOpen(false)
        console.log(err);
      });
    
  };

  return (
    <>
      <div className="all-record">
        <Table
          columns={columns}
          dataSource={currentData}
          className="table-data"
          scroll={{ x: "max-content" }}
          pagination={false}
        />
        <div className="pagination-bar">
          <Pagination
            defaultCurrent={currentPage}
            total={recordPerPage}
            onChange={(e) => fetchData(e)}
            showSizeChanger={false}
          />
        </div>
      </div>
      <Loading open={open} setOpen={setOpen} />
    </>
  );
};

export default DataList;
