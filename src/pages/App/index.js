import React from "react";
import { Route, Switch, Redirect } from "react-router-dom";
import { routersAuth } from "../../configs";

const App = (props) => {
  return (
    <React.Suspense fallback={null}>
      <Switch>
        {routersAuth.map((route, idx) => {
          return (
            <Route
              key={idx}
              exact={route.exact}
              path={route.path}
              render={() => {
                const Component = React.lazy(() =>
                  import(`../../pages/${route.component}`)
                );
                return <Component {...props} route={route} />;
              }}
            />
          );
        })}
        <Redirect to="/all-record" />
      </Switch>
    </React.Suspense>
  );
};

export default App