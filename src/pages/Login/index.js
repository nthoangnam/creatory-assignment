import {
  faFacebook,
  faGoogle,
  faTwitter,
} from "@fortawesome/free-brands-svg-icons";
import { FontAwesomeIcon } from "@fortawesome/react-fontawesome";
import { Form, Input, notification } from "antd";
import React, { useState } from "react";
import "./index.scss";
import axios from "axios";
import { useHistory } from "react-router-dom";

const Login = () => {
  const [username, setUserName] = useState("");
  const [password, setPassword] = useState("");
  const [loading, setLoading] = useState(false);

  let history = useHistory();

  const openNotificationWithIcon = (type, title, des) => {
    notification[type]({
      message: title,
      description: des,
      placement: "bottomRight",
    });
  };

  const handleLogin = () => {
    if (username !== "" || password !== "") {
      var config = {
        method: "post",
        url: "http://localhost:8888/auth",
        data: {
          username: username.trim(),
          password: password,
        },
        headers: {},
      };
      setLoading(true);
      axios(config)
        .then(function (response) {
          setLoading(false);
          if (response.data.authenticated === true) {
            localStorage.setItem("access-token", "token");
            openNotificationWithIcon(
              "success",
              "Success",
              "Login successfully!"
            );
            history.push("/all-record");
          }
        })
        .catch(function (error) {
          setLoading(false);
          if (error.response.status === 401) {
            openNotificationWithIcon(
              "error",
              "Error",
              "Username or password is incorrect!"
            );
          }
        });
    }
  };

  return (
    <div className="login-page">
      <div className="limiter">
        <div className="container-login100">
          <div className="wrap-login100 p-l-55 p-r-55 p-t-50 p-b-54">
            <Form name="basic" layout="vertical">
              <span className="login100-form-title p-b-49">Login</span>

              <div className="wrap-input100 validate-input m-b-10">
                <Form.Item
                  label={<span className="label-input100">Username</span>}
                  name="username"
                  rules={[
                    {
                      required: true,
                      message: "Please input your username!",
                    },
                  ]}
                >
                  <Input
                    className="input100"
                    placeholder="Type your username"
                    onChange={(e) => {
                      setUserName(e.target.value);
                    }}
                  />
                </Form.Item>
              </div>
              <div className="wrap-input100 validate-input">
                <Form.Item
                  label={<span className="label-input100">Password</span>}
                  name="password"
                  rules={[
                    {
                      required: true,
                      message: "Please input your password!",
                    },
                  ]}
                >
                  <Input.Password
                    className="input100"
                    type="password"
                    placeholder="Type your password"
                    onPressEnter={() => handleLogin()}
                    onChange={(e) => {
                      setPassword(e.target.value);
                    }}
                  />
                </Form.Item>
              </div>
              <div className="text-right p-t-8 p-b-31">
                <button href="#">Forgot password?</button>
              </div>
              <div className="container-login100-form-btn">
                <div className="wrap-login100-form-btn">
                  <div className="login100-form-bgbtn" />
                  <button
                    htmltype="submit"
                    className="login100-form-btn"
                    onClick={() => handleLogin()}
                    onPressEnter={() => handleLogin()}
                    disabled={loading}
                  >
                    Login
                  </button>
                </div>
              </div>
              <div className="txt1 text-center p-t-54 p-b-20">
                <span>Or Sign Up Using</span>
              </div>
              <div className="flex-c-m">
                <button className="login100-social-item bg1">
                  <FontAwesomeIcon icon={faFacebook} />
                </button>
                <button className="login100-social-item bg2">
                  <FontAwesomeIcon icon={faTwitter} />
                </button>
                <button className="login100-social-item bg3">
                  <FontAwesomeIcon icon={faGoogle} />
                </button>
              </div>
              <div className="flex-col-c p-t-155">
                <span className="txt1 p-b-17">Have not account yet?</span>
                <button className="txt2">
                  Sign Up
                </button>
              </div>
            </Form>
          </div>
        </div>
      </div>
    </div>
  );
};

export default Login;
