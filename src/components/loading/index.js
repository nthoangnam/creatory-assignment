import React from "react";
import { Modal } from "antd";

const Loading = (props) => {
  const { open, setOpen } = props;
  return (
    <Modal
      title="Basic Modal"
      visible={open}
      footer={() => {}}
      centered
      onCancel={() => setOpen(false)}
      maskClosable={false}
    >
      <div class="lds-ellipsis">
        <div></div>
        <div></div>
        <div></div>
        <div></div>
      </div>
    </Modal>
  );
};

export default Loading;
