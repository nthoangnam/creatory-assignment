import React from "react";
import "./App.css";
import "antd/dist/antd.css"; // or 'antd/dist/antd.less'
import Router from "./pages/Routes";
import './App.scss'

function App() {
  return (
    <div className="App">
      <Router />
    </div>
  );
}

export default App;
