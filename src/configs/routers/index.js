export const routersNotAuth = [
  {
    exact: true,
    path: "/login",
    component: "Login",
  },
];

export const routersAuth = [
  {
    exact: true,
    path: "/all-record",
    component: "DataList",
    root: true,
    requirementPermission: [],
  },
  {
    exact: true,
    path: "/dashboard",
    component: "Dashboard",
    root: true,
    requirementPermission: [],
  }
];
